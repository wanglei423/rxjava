import rx.Observable;
import rx.Subscriber;
import rx.functions.Action1;
import rx.functions.Func1;
import rx.schedulers.Schedulers;

public class test {

    Observable<String> myOb = Observable.create(
                                                new Observable.OnSubscribe<String>() {
                                                    @Override
                                                    public void call(Subscriber<? super String> sub) {
                                                        sub.onNext("Hello, world");
                                                        sub.onNext("my, world");
                                                        sub.onCompleted();
                                                    }
                                                }
                                                );
    Subscriber<String> mySub = new Subscriber<String>() {
            @Override
            public void onNext(String s)
            { System.out.println(Thread.currentThread().getName() + " sub1 " + s);}            

            @Override
            public void onCompleted() { System.out.println("sub1 onCompleted()" );}

            @Override
            public void onError(Throwable e) {}
        };

    Subscriber<String> mySub2 = new Subscriber<String>() {
            @Override
            public void onNext(String s)
            { System.out.println(Thread.currentThread().getName() + " sub2 " + s);}
            @Override
            public void onCompleted() {System.out.println("sub2 onCompleted()" );}
            @Override
            public void onError(Throwable e) {}
            
        };

    public void test() {
        myOb.subscribe(mySub);
        myOb.subscribe(mySub2);
        myOb.subscribe(mySub);
    }

    public void test2() {
        Observable<String> mOb = Observable.just("this is a simple world");
        mOb.subscribe(new Action1<String>() {
                @Override
                public void call(String s) {
                    System.out.println("test2:" + s);
                }
            });
    }

    public void test3() {
        String[] nums = {"1", "2", "3", "3", "4", "6"};
        Observable.from(nums)
            .map(new Func1<String, Integer>() {
                    @Override
                    public Integer call(String s) {
                        return Integer.parseInt(s);
                    }
                })
            .distinct()
            .filter(new Func1<Integer, Boolean>() {
                    @Override
                    public Boolean call(Integer integer) {
                        System.out.println("filter thread: " + Thread.currentThread().getName());
                        return integer > 1;
                    }
                })
            .take(3)
            .observeOn(Schedulers.newThread())
            .subscribe(new Action1<Integer>() {
                    @Override
                    public void call(Integer integer) {
                        System.out.println("current thread: " + Thread.currentThread().getName());
                        System.out.println("current number: " + integer);
                    }
                });
    }

    public void test4() {
        Observable.just(0,1,2,3,3,4,6,7,100)
            .filter(new Func1<Integer, Boolean>() {
                    @Override
                    public Boolean call(Integer integer) {
                        return integer > 5;
                    }
                }
                )
            .subscribe(new Action1<Integer>() {
                    @Override
                    public void call(Integer integer) {
                        System.out.println("current number: " + integer);
                    }
                }
                );
    }

    public void test5() {
        String[][] arrs = {{"11", "12"}, {"21", "22"}, {"31", "32"}};
        Action1<String> action1 = new Action1<String>() {
                @Override
                public void call(String s) {
                    System.out.println(s);
                }
            };

        Observable
            .from(arrs)
            .flatMap(new Func1<String[], Observable<String>>() {
                    @Override
                    public Observable<String> call(String[] strings) {
                        return Observable.from(strings);
                    }
                })
            .subscribe(action1);
    }
    
    public static void  main(String args[]) {
        //System.out.println("hello world");
        new test().test5();
    }
}
